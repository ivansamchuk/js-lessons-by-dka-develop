// let a = 2;
// let b = 4;
// console.log(a + b);

/**


// Example 0
const list = document.getElementById('list');
console.log(list);

console.log(list.getElementsByTagName('li'));



// Example 1
const list = document.getElementById('list');

setTimeout(() => {
    const item = document.createElement('li');
    item.innerHTML = "<span>item6</span>";
    item.style.color = 'aqua';
    list.appendChild(item);
}, 2000);



// Example 2
const list = document.getElementById('list');

console.log(list.childNodes);     // log all elements that are lists

console.log(list.firstChild);

console.log(list.dataset.list);     // значення data-list елемента


// Example 3

const button = document.getElementById('button');

const greatJob = (evt) => {
    alert(`'Great job!' - said ${evt.target.name}`);
};

// Example 4

const handleLinkClick = evt => {
    evt.preventDefault();
    evt.stopPropagation();
    return false;
};


// Example 5

let event = new Event('alarm');
let button = document.getElementById('button');

button.addEventListener('click', evt =>{
    button.dispatchEvent(event);
});

button.addEventListener('alarm', evt =>{
    evt.target.innerHTML = 'Alarm!';
});
**/

// Example 6 Add your events

const list = document.getElementById('list');
const elements = document.getElementsByTagName('li')


for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener('click', evt => {
        console.log(evt.target.innerHTML);
    });
}


const d1 = document.getElementById('d1');
const d2 = document.getElementById('d2');
const d3 = document.getElementById('d3');

d1.addEventListener('click', () => {
    console.log('d1');
});

d2.addEventListener('click', (evt) => {
    console.log('d2');
    evt.stopPropagation();
});

d3.addEventListener('click', () => {
    console.log('d3');
});

